from django.apps import AppConfig


class TasksConfig(AppConfig):
    name = 'tasks'
    start_task_status = "open"
