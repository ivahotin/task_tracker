# -*- coding: utf-8 -*-

from rest_framework.response import Response
from rest_framework import generics
from rest_framework import mixins
from rest_framework.decorators import api_view

from .models import Task
from .models import TaskStatus
from .serializers import TaskSerializer
from .exceptions import ChangeStatusNotAllowed


class TaskView(
    mixins.DestroyModelMixin,
    mixins.UpdateModelMixin,
    generics.ListCreateAPIView
):
    u"""Вью для работы с задачами."""

    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    def post(self, request, *args, **kwargs):

        if request.user.is_authenticated() and request.user.has_perm(
                "auth.create"):
            return super(TaskView, self).post(request, *args, **kwargs)
        else:
            return Response(
                status=403, data={"error": "Create task not allowed"})

    def delete(self, request, *args, **kwargs):

        if request.user.is_authenticated() and request.user.has_perm(
                "auth.delete"):
            return self.destroy(request, *args, **kwargs)
        else:
            return Response(
                status=403, data={"error": "Delete task not allowed"})

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)


@api_view(["POST"])
def change_status(request, task_id):
    u"""Смена статуса задачи."""

    try:
        to_status = TaskStatus.objects.get(id=request.data.get("status"))
    except KeyError:
        return Response(
            status=500, data={"error": "status must be specified."})
    except TaskStatus.DoesNotExist:
        return Response(status=500, data={"error": "Task status not found."})

    try:
        task = Task.objects.get(id=task_id)
    except Task.DoesNotExist:
        return Response(status=500, data={"error", "Task not found"})

    try:
        task.change_status(request.user, to_status)
    except ChangeStatusNotAllowed as e:
        return Response(status=403, data={"error": e.message})

    return Response(status=200)
