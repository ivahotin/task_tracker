# -*- coding: utf-8 -*-


from rest_framework import serializers

from .models import Task
from .models import TaskStatus


class TaskSerializer(serializers.ModelSerializer):
    u"""Сериализатор для задач."""

    id = serializers.IntegerField(required=False)
    title = serializers.CharField(max_length=20)
    description = serializers.CharField()
    status_id = serializers.IntegerField(required=False)

    def create(self, validated_data):
        u"""Создание задачи."""

        try:
            start_task_status = TaskStatus.objects.get(
                name=TaskStatus.TASK_START_STATUS)
        except TaskStatus.DoesNotExist:
            raise

        validated_data["status"] = start_task_status

        return Task.objects.create(**validated_data)

    class Meta:
        model = Task
        fields = ('id', 'title', 'description', 'status_id')
