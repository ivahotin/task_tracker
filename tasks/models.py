# coding: utf-8

from __future__ import unicode_literals

from django.db import models

from .apps import TasksConfig
from .exceptions import ChangeStatusNotAllowed


class TaskStatus(models.Model):
    u"""Статус задачи.

    У модели имеется набор статусов по-умолчанию:
        * open
        * in_progress
        * finished
        * testing
        * closed
    Они могут быть загружены из фикстуры /tasks/fixtures/default_statuses.json.
    """

    # Начальный статус задачи.
    TASK_START_STATUS = TasksConfig.start_task_status

    name = models.CharField(max_length=10)


class Task(models.Model):
    """Модель задачи.

    Содержит название задачи, описание и статус.
    Статус является отдельной моделью, для возможности быстрого расширения,
    при необходимости добавить новый статус в процессе эксплуатации.
    """

    title = models.CharField(max_length=20)
    description = models.TextField()

    status = models.ForeignKey("tasks.TaskStatus")

    def change_status(self, user, to_status):
        u"""Метод смены статуса задачи.

        :param to_status: Статус задачи.
        """

        current_status = self.status
        perm_code = "auth.move:{0}-{1}".format(
            current_status.name, to_status.name)

        if not user.has_perm(perm_code):
            raise ChangeStatusNotAllowed("Change status not allowed")

        self.status = to_status
        self.save()
