# -*- coding: utf-8 -*-


class ChangeStatusNotAllowed(Exception):
    u"""Исключение при невозможности смены статуса задачи."""
