# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.contrib.auth.models import Group
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType


class Command(BaseCommand):
    help = 'Load initial data'

    def handle(self, *args, **options):

        content_type = ContentType(app_label="auth", model="Group")
        content_type.save()

        create_perm = Permission.objects.create(
            name=u"Создание", content_type=content_type, codename="create")
        delete_perm = Permission.objects.create(
            name=u"Удаление", content_type=content_type, codename="delete")

        move_to_in_progress_perm = Permission.objects.create(
            name=u"Open2InProgress", content_type=content_type,
            codename="move:open-in_progress")
        move_to_finished = Permission.objects.create(
            name=u"InProgress2Finished", content_type=content_type,
            codename="move:in_progress-finished")
        move_to_testing = Permission.objects.create(
            name=u"Finished2Testing", content_type=content_type,
            codename="move:finished-testing")
        move_to_closed = Permission.objects.create(
            name=u"Testing2Closed", content_type=content_type,
            codename="move:testing-closed")
        move_to_open = Permission.objects.create(
            name=u"Testing2Open", content_type=content_type,
            codename="move:testing-open"
        )

        Group.objects.get(name=u"Менеджер").permissions.add(create_perm)
        Group.objects.get(name=u"Менеджер").permissions.add(delete_perm)

        Group.objects.get(name=u"Аналитик").permissions.add(create_perm)
        Group.objects.get(name=u"Аналитик").permissions.add(delete_perm)

        Group.objects.get(name=u"Разработчик").permissions.add(
            move_to_in_progress_perm)
        Group.objects.get(name=u"Разработчик").permissions.add(
            move_to_finished)
        Group.objects.get(name=u"Тестировщик").permissions.add(move_to_testing)
        Group.objects.get(name=u"Тестировщик").permissions.add(move_to_closed)
        Group.objects.get(name=u"Тестировщик").permissions.add(move_to_open)