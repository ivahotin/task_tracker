var app = angular.module("app",
    ["ngRoute", "ngResource", "ngCookies", "ui.bootstrap", "gridster"]);


app.config(['$httpProvider', function($httpProvider){
        // django and angular both support csrf tokens. This tells
        // angular which cookie to add to what header.
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    }
]);


app.controller("DashboardCtrl", [
    '$scope', '$rootScope', '$timeout', '$http', '$cookieStore', 'TaskService',
    function($scope, $rootScope, $timeout, $http, $cookieStore, TaskService) {

    /*
     * Настройки гридстера.
     */
    $scope.gridsterOptions = {
            margins: [20, 20],
            columns: 5,
            draggable: {
                handle: 'h3',
                start: function(event, $element, task) {
                    $cookieStore.put(task.$$hashKey + "-from_status", task.col);
                },
                drag: function(event, $element, task) {},
                stop: function(event, $element, task) {
                    var fromStatus = $cookieStore.get(
                        task.$$hashKey + "-from_status");
                    if (task.id) {
                        TaskService.changeStatus(task, task.col + 1).then(
                            function(result){
                                var fromStatus = $cookieStore.get(
                                    task.$$hashKey + "-from_status");
                                task.col = fromStatus;
                            }
                        );
                    } else {
                        task.col = fromStatus;
                        alert("Save before change status");
                    }
                }
            },
            resizable: {enabled: false}
    };

    $http.get('/tasks/').then(
        function(response){
            if (response.status == 200){
                $scope.dashboard = {
                    tasks: []
                };

                response.data.forEach(
                    function(task, task_num, tasks){
                        $scope.dashboard.tasks.push(
                            {
                                title: task.title,
                                description: task.description,
                                id: task.id,
                                sizeX: 1,
                                sizeY: 1,
                                col: task.status_id - 1,
                                row: 0
                            }
                        )
                    }
                );
            }
        }
    );

    // Удаляем все задачи не сохранненые в БД (объекты без ID).
    $scope.clear = function() {
        $scope.dashboard.tasks = _.filter($scope.dashboard.tasks,
            function(task){
                if (task.id) {return true;}
            }
        );
    };

    // Добавление задачи на рабочий стол.
    $scope.addTask = function() {
        $scope.dashboard.tasks.push({
            title: "New Task",
            description: null,
            col: 0,
            row: 0,
            sizeX: 1,
            sizeY: 1
        });
    }

    TaskService.getPerms().then(function(result){
        $scope.canAdd = result.can_create;
        $scope.canDelete = result.can_delete;
    });
}]);


/*
 * Сервис для работы с задачами.
 */
app.factory("TaskService", function($http, $modal, $q, $cookieStore){

    /*
     * Функция удаляет заданный таск.
     * @param {string} taskID - идентификатор таска.
     * @param {string} taskID - список задач
     */
    function doDelete(task, tasks){
        // Запись уже в БД. Необходимо отправить запрос на удаление.
        if (task.id){
            $http.delete("/tasks/" + task.id).then(function(response){
                // 204 No Content
                if (response.status == 204){
                    tasks.splice(tasks.indexOf(task), 1);
                }
            }, function(response){
                alert(response.data.error);
            });
        } else {
            // Задача только добавлена на рабочий стол, но не сохранена в БД.
            tasks.splice(tasks.indexOf(task), 1);
        }
    }

    /*
     * Обновление данных о задаче.
     * @param {object} task - данные о задаче.
     */
    function update(task){

        var deferred = $q.defer();

        $http.put("/tasks/" + task["id"], {
            title: task["title"],
            description: task["description"],
            status_id: task["col"] + 1
        }).then(
            function(response){
                if (response.status == 200){
                    deferred.resolve(response.data);
                }
            }
        );

        return deferred.promise;
    }

    /*
     * Создание задачи.
     * @param {object} task - данные о задаче.
     */
    function create(task){

        var deferred = $q.defer();

        $http.post("/tasks/", {
            id: task["id"],
            title: task["title"],
            description: task["description"]
        }).then(
            function(response){
                if (response.status == 201){
                    deferred.resolve(response.data);
                }
            },
            function(response){
                alert(response.data.error);
            }
        );

        return deferred.promise;
    }

    /*
     * Смена статуса задачи.
     */
    function changeStatus(task, toStatus){

        var deferred = $q.defer();

        $http.post("/status/" + task.id, {
            status: toStatus
        }).then(function(result){}, function(result){
            alert(result.data.error);
            deferred.resolve(result.data);
        });

        return deferred.promise;
    }

    /*
     * Функция возвращает набор прав пользователя.
     */
    function getPerms(){

        var deferred = $q.defer();
        $http.get("/perms/").then(function(result){
            deferred.resolve(result.data);
        });
        return deferred.promise;
    }

    return {
        doDelete: doDelete,
        create: create,
        update: update,
        changeStatus: changeStatus,
        getPerms: getPerms,
    }
});


app.controller('CustomWidgetCtrl', [
    '$scope', '$rootScope', '$modal', '$cookieStore', 'TaskService',
    function($scope, $rootScope, $modal, $cookieStore, TaskService) {

        $scope.remove = function(task) {
            TaskService.doDelete(task, $scope.dashboard.tasks);
        };

        $scope.openSettings = function(task) {
            $modal.open({
                scope: $scope,
                templateUrl: '/static/html/view/task_details.html',
                controller: 'WidgetSettingsCtrl',
                resolve: {
                    task: function() {
                        return task;
                    }
                }
            });
        };

        TaskService.getPerms().then(function(result){
            $scope.canAdd = result.can_create;
            $scope.canDelete = result.can_delete;
        });
    }
]);


app.controller('WidgetSettingsCtrl', [
    '$scope', '$timeout', '$rootScope', '$modalInstance', '$http', 'task',
    'TaskService',
    function($scope, $timeout, $rootScope, $modalInstance, $http, task, TaskService) {
        $scope.task = task;

        $rootScope.canAdd = $scope.canAdd;
        $rootScope.canDelete = $scope.canDelete;

        $scope.form = {
            id: task.id,
            title: task.title,
            description: task.description,
            sizeX: 1,
            sizeY: 1,
            col: task.col,
            row: task.row
        };

        $scope.dismiss = function() {
            $modalInstance.dismiss();
        };

        $scope.remove = function() {
            TaskService.doDelete($scope.task, $scope.dashboard.tasks);
            $modalInstance.close();
        };

        var saveTaskCallback = function(result){
            angular.extend(task, $scope.form);
            if (!task.id){
                task.id = result.id;
            }
        }

        $scope.submit = function() {
            if ($scope.form["id"]){
                TaskService.update($scope.form).then(saveTaskCallback);
            } else {
                TaskService.create($scope.form).then(saveTaskCallback);
            }
            $modalInstance.close(task);
        };

    }
]);


app.factory('AuthService', function($resource){
        function add_auth_header(data, headersGetter){
            // as per HTTP authentication spec [1], credentials must be
            // encoded in base64. Lets use window.btoa [2]
            var headers = headersGetter();
            headers['Authorization'] = ('Basic ' + btoa(data.username +
                                        ':' + data.password));
        }
        // defining the endpoints. Note we escape url trailing dashes: Angular
        // strips unescaped trailing slashes. Problem as Django redirects urls
        // not ending in slashes to url that ends in slash for SEO reasons, unless
        // we tell Django not to [3]. This is a problem as the POST data cannot
        // be sent with the redirect. So we want Angular to not strip the slashes!
        return {
            auth: $resource('/api/auth/', {}, {
                login: {method: 'POST', transformRequest: add_auth_header},
                logout: {method: 'DELETE'}
            })
        };
});


app.controller('LoginController', function($scope, $rootScope, $location, $cookies, $cookieStore, AuthService) {
        // Angular does not detect auto-fill or auto-complete. If the browser
        // autofills "username", Angular will be unaware of this and think
        // the $scope.username is blank. To workaround this we use the
        // autofill-event polyfill [4][5]
        $('#id_auth_form input').checkAndTriggerAutoFillEvent();

        $scope.getCredentials = function(){
            return {username: $scope.username, password: $scope.password};
        };

        $scope.login = function(){
            AuthService.auth.login($scope.getCredentials()).
                $promise.
                    then(function(response){
                        // on good username and password
                        $scope.user = response.username;
                        $cookieStore.put("user", response.username);
                        $location.path("/list");
                    }).
                    catch(function(response){
                        // on incorrect username and password
                        $scope.alert = {
                            type: "danger",
                            msg: response.data.detail
                        };
                    });
        };

        $scope.closeAlert = function() {$scope.alert = {};}

        $scope.register = function(){$location.path("/register");}
    });

/*
 * Сервис, отвечающий за функционал создания нового пользователя.
 */
app.factory("RegisterService", function($http, $location, $rootScope, $q){

    /*
     * Функция выполняющая валидацию данных, которые используются для
     * создания нового пользователя.
     * @param {string} login - логин пользователя
     * @param {string} password - пароль пользователя
     * @param {string} password_confirmation - подтверждение пароля.
     * @param {int} role_id - идентификатор роли
     */
    function validate(login, password, password_confirmation, role_id){

        var errors = [];

        if (!login) { errors.push("Login is required."); }

        if (!password || !password_confirmation){
            errors.push("Password and confirmation are required.");
        } else {
            if (password !== password_confirmation){
                errors.push("Password and his confirmation must be same.");
            }
            if (password.length < 6){
                errors.push("Password length must be >= 6.");
            }
        }

        if (!role_id) { errors.push("Role must be selected."); }

        return errors;
    }

    /*
     * Функция выполняющая создание нового пользователя системы.
     * Выполняет валидацию формы и отправку запроса на бэкенд.
     * @param {string} login - логин пользователя
     * @param {string} password - пароль пользователя
     * @param {int} role_id - идентификатор роли
     */
    function register(login, password, role_id){

        var deferred = $q.defer();

        $http.post("/users/", {
            username: login,
            password: password,
            role_id: role_id
        }).then(
            function(response){
                if (response.status == 201){
                    $location.path("/login");
                } else {
                    deferred.resolve(response.data);
                }
            }
        );

        return deferred.promise;
    }

    return {
        validate: validate,
        register: register
    }
});


/*
 * Контроллер для вьюхи регистрации.
 * Выполняет подгрузку существующих ролей (Разработчик, Аналитик и т.д).
 * Выполняет создание пользователя путем делегирования сервису RegisterService.
 */
app.controller("RegisterController", function($scope, $location, $http, RegisterService){
    /*
     * Роли пользователей в <select> заполняются динамически.
     */
    $http.get("/roles/").success(function(data){$scope.roles = data;});

    $scope.register = function() {

        var errors = RegisterService.validate(
            $scope.username, $scope.password,
            $scope.password_confirmation, $scope.selected_role);

        if (errors.length){
            $scope.alert = {
                type: "danger",
                msg: errors.join("\n")
            };
            return;
        }

        RegisterService.register($scope.username, $scope.password, $scope.selected_role["id"]).then(function(result){
            $scope.alert = {
                type: "danger",
                msg: result
            };
        });
    }

    $scope.closeAlert = function() {$scope.alert = {};}
});


/*
 * Конфигурирование роутинга.
 */
app.config(function($routeProvider){
    // Главная вьюха.
    $routeProvider.when("/list", {
        templateUrl: "/static/html/view/list.html",
        controller: "DashboardCtrl"
    });
    // Вьюха логинки.
    $routeProvider.when("/login", {
        templateUrl: "/static/html/view/login.html",
        controller: "LoginController"
    });
    // Вьюха регистрации.
    $routeProvider.when("/register", {
        templateUrl: "/static/html/view/register.html",
        controller: "RegisterController"
    });

    $routeProvider.otherwise({redirectTo: "login"});
});