# -*- coding: utf-8 -*-

from django.contrib.auth.models import Group
from django.db import IntegrityError
from django.utils.decorators import method_decorator
from django.contrib.auth import login
from django.contrib.auth import logout
from django.core.exceptions import ValidationError
from django.views.decorators.csrf import csrf_exempt

from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view

from .serializers import RolesSerializer
from .serializers import UserSerializer
from .auth import QuietBasicAuthentication


class RolesListView(generics.ListCreateAPIView):
    u"""Список ролей."""

    queryset = Group.objects.all()
    serializer_class = RolesSerializer


class CreateUserView(generics.CreateAPIView):
    u"""Вью создания пользователя.

    Вызывается при регистрации.
    С клиента приходят логин, пароль, роль пользователя.
    """

    serializer_class = UserSerializer

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        try:
            return super(CreateUserView, self).post(request, *args, **kwargs)
        except (IntegrityError, ValidationError):
            return Response(u"Не удалось создать пользователя")


class AuthView(APIView):
    u"""Вью для логгирования и разлоггирования."""

    authentication_classes = (QuietBasicAuthentication,)

    def post(self, request):
        login(request, request.user)
        return Response(UserSerializer(request.user).data)

    def delete(self, request):
        logout(request)
        return Response({})


@api_view(["GET"])
def get_perms(request):
    u"""Вью, возвращающее набор прав пользователя."""

    if request.user.is_authenticated():
        can_delete = request.user.has_perm("auth.delete")
        can_create = request.user.has_perm("auth.create")
        return Response({"can_delete": can_delete, "can_create": can_create})

    return Response(status=403, data={"error": "Need auth."})
