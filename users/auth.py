# -*- coding: utf-8 -*-

from rest_framework.authentication import BasicAuthentication


class QuietBasicAuthentication(BasicAuthentication):
    u"""Кастомная аутентификатор.

    Реализация позаимстована https://richardtier.com/2014/03/06/110/.
    """

    def authenticate_header(self, request):
        return 'xBasic realm="%s"' % self.www_authenticate_realm
