# -*- coding: utf-8 -*-

from datetime import date

from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from rest_framework import serializers


class RolesSerializer(serializers.ModelSerializer):
    u"""Сериализатор для ролей."""

    class Meta:
        model = Group
        fields = ('id', 'name')


class UserSerializer(serializers.Serializer):
    u"""Сериализатор для пользователей.

    Используется при регистрации и авторизации.
    """

    username = serializers.CharField(
        required=True, allow_blank=False, max_length=30)
    password = serializers.CharField(
        required=True, allow_blank=False, max_length=128)
    role_id = serializers.IntegerField(required=False)

    def create(self, validated_data):
        u"""Регистрация пользователя.

        Включает в себя создание пользователя и добавление его в группу.
        """

        user = User(
            date_joined=date.today(),
            username=validated_data["username"]
        )
        user.set_password(validated_data["password"])
        user.save()

        try:
            group = Group.objects.get(id=validated_data["role_id"])
        except Group.DoesNotExist:
            raise ValidationError(u"Роль не найдена.")

        user.groups.add(group)

        return user
