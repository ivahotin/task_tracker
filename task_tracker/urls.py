"""task_tracker URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url
from django.contrib import admin
from django.contrib.staticfiles import urls as static_urls

from users.views import RolesListView
from users.views import CreateUserView
from users.views import AuthView
from users.views import get_perms

from tasks.views import TaskView
from tasks.views import change_status

from .views import WorkSpace


urlpatterns = [
    url(r'^$', WorkSpace.as_view()),
    url(r'^admin/', admin.site.urls),
    url(r'^roles/', RolesListView.as_view()),
    url(r'^users/', CreateUserView.as_view()),
    url(r'^tasks/$', TaskView.as_view()),
    url(r'^tasks/(?P<pk>[0-9]+)$', TaskView.as_view()),
    url(r'^status/(?P<task_id>[0-9]+)$', change_status),
    url(r'^perms/$', get_perms),
    url(r'^api/auth', AuthView.as_view())
] + static_urls.staticfiles_urlpatterns()
