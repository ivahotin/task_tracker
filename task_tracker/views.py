

from django.conf import settings
from django.views.generic import TemplateView


class WorkSpace(TemplateView):

    template_name = "index.html"

    def get_context_data(self, **kwargs):
        kwargs["static_path"] = settings.STATIC_URL

        return kwargs
